<?php
/**
 * Customizer
 *
 * @package      Bootstrap for Genesis
 * @since        1.0
 * @link         http://webdevsuperfast.github.io
 * @author       Rotsen Mark Acob <webdevsuperfast.github.io>
 * @copyright    Copyright (c) 2017, Rotsen Mark Acob
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
*/

add_action( 'customize_register', function( $wp_customize ) {
    // Add Default Settings
    $wp_customize->add_setting( 'bootstrap-for-genesis', array(
        'capability' => 'edit_theme_options',
        'type' => 'theme_mod'
    ) );

    // Add Bootstrap Panel
    $wp_customize->add_panel( 'bootstrap', array(
        'title' => __( 'Site Appearence', 'bootstrap-for-genesis' ),
        'priority' => 100
    ) );

    // Add Navigation Section
    $wp_customize->add_section( 'navigation', array(
        'title' => __( 'Navigation Settings', 'bootstrap-for-genesis' ),
        'priority' => 10,
        'panel' => 'bootstrap'
    ) );

    //* Add Navigation Controls
    $wp_customize->add_setting( 'navposition', array(
        'default' => 'block'
    ) );

    $wp_customize->add_control( 'navposition', array(
        'type' => 'select',
        'priority' => 10,
        'label' => __( 'Navigation Position', 'bootstrap-for-genesis' ),
        'section' => 'navigation',
        'choices' => array(
            'block' => __( 'Block', 'bootstrap-for-genesis' ),
            'sticky-top' => __( 'Sticky Top', 'bootstrap-for-genesis' ),
            'fixed-top' => __( 'Fixed Top', 'bootstrap-for-genesis' ),
            'fixed-bottom' => __( 'Fixed Bottom', 'bootstrap-for-genesis' ),
        )
    ) );

    $wp_customize->add_setting( 'navcontainer', array(
        'default' => 'lg'
    ) );

    // Navigation Container
    $wp_customize->add_control( 'navcontainer', array(
        'type' => 'select',
        'priority' => 20,
        'label' => __( 'Navigation Container', 'bootstrap-for-genesis' ),
        'section' => 'navigation',
        'choices' => array(
            'sm' => __( 'Small', 'bootstrap-for-genesis' ),
            'md' => __( 'Medium', 'bootstrap-for-genesis' ),
            'lg' => __( 'Large', 'bootstrap-for-genesis' ),
            'xl' => __( 'Extra Large', 'bootstrap-for-genesis' )
        )
    ) );

    // Navigation Color
    $wp_customize->add_setting( 'navcolor', array(
        'default' => 'dark'
    ) );

    $wp_customize->add_control( 'navcolor', array(
        'type' => 'select',
        'priority' => 30,
        'label' => __( 'Navigation Background', 'bootstrap-for-genesis' ),
        'section' => 'navigation',
        'choices' => array(
            'light' => __( 'Light', 'bootstrap-for-genesis' ),
            'dark' => __( 'Dark', 'bootstrap-for-genesis' ),
            'primary' => __( 'Primary', 'bootstrap-for-genesis' ),
            'secondary' => __( 'Secondary', 'bootstrap-for-genesis' ),
            'white' => __( 'White', 'bootstrap-for-genesis')
        )
    ) );

    // Navigation Extras
    $wp_customize->add_setting( 'navextra', array(
        'default' => 'cta'
    ) );

    $wp_customize->add_control( 'navextra', array(
        'type' => 'select',
        'priority' => 40,
        'label' => __( 'Navigation Extras', 'bootstrap-for-genesis' ),
        'section' => 'navigation',
        'choices' => array(
            '' => __( 'None', 'bootstrap-for-genesis' ),
            'date' => __( 'Date', 'bootstrap-for-genesis' ),
            'search' => __( 'Search Form', 'bootstrap-for-genesis' ),
            'cta' => __( 'Call to Action', 'bootstrap-for-genesis' ),
        )
    ) );

    // Footer Section
    $wp_customize->add_section( 'footer', array(
        'title' => __( 'Footer Section', 'bootstrap-for-genesis' ),
        'priority' => 40,
        'panel' => 'bootstrap'
    ) );

    $wp_customize->add_setting( 'footerwidgetbg', array(
        'default' => 'dark'
    ) );

    $wp_customize->add_control( 'footerwidgetbg', array(
        'type' => 'select',
        'priority' => 30,
        'label' => __( 'Footer Widget Background', 'bootstrap-for-genesis' ),
        'section' => 'footer',
        'choices' => array(
            'light' => __( 'Light', 'bootstrap-for-genesis' ),
            'dark' => __( 'Dark', 'bootstrap-for-genesis' ),
            'primary' => __( 'Primary', 'bootstrap-for-genesis' ),
            'secondary' => __( 'Secondary', 'bootstrap-for-genesis' ),
            'white' => __( 'White', 'bootstrap-for-genesis')
        )
    ) );

    $wp_customize->add_setting( 'footerbg', array(
        'default' => 'dark'
    ) );

    $wp_customize->add_control( 'footerbg', array(
        'type' => 'select',
        'priority' => 30,
        'label' => __( 'Footer Background', 'bootstrap-for-genesis' ),
        'section' => 'footer',
        'choices' => array(
            'light' => __( 'Light', 'bootstrap-for-genesis' ),
            'dark' => __( 'Dark', 'bootstrap-for-genesis' ),
            'primary' => __( 'Primary', 'bootstrap-for-genesis' ),
            'success' => __( 'Secondary', 'bootstrap-for-genesis' ),
            'white' => __( 'White', 'bootstrap-for-genesis')
        )
    ) );

    $wp_customize->add_section( 'home-section', array(
        'title' => __( 'Homepage', 'bootstrap-for-genesis' ),
        'priority' => 80,
        'panel' => 'bootstrap'
    ) );


    $wp_customize->add_setting('pres', array(
        'default' => 'Mi chiamo Massimo e sono un ottico'
    ));

    $wp_customize->add_control('pres', array(
        'type' => 'text',
        'label' => __( 'Presentazione', 'bootstrap-for-genesis' ),
        'section' => 'home-section'
    ));

    $wp_customize->add_setting('lupi-link', array(
        'default' => '/il-branco'
    ));

    $wp_customize->add_control('lupi-link', array(
        'type' => 'text',
        'label' => __( 'Link a pagina branco', 'bootstrap-for-genesis' ),
        'section' => 'home-section'
    ));

    $wp_customize->add_setting('esplo-link', array(
        'default' => '/il-riparto'
    ));

    $wp_customize->add_control('esplo-link', array(
        'type' => 'text',
        'label' => __( 'Link a pagina riparto', 'bootstrap-for-genesis' ),
        'section' => 'home-section'
    ));

    $wp_customize->add_setting('clan-link', array(
        'default' => '/il-clan'
    ));

    $wp_customize->add_control('clan-link', array(
        'type' => 'text',
        'label' => __( 'Link a pagina clan', 'bootstrap-for-genesis' ),
        'section' => 'home-section'
    ));

    $wp_customize->add_setting('link-button-pages', array(
        'default' => 'Scopri'
    ));

    $wp_customize->add_control('link-button-pages', array(
        'type' => 'text',
        'label' => __( 'Testo bottoni branche', 'bootstrap-for-genesis' ),
        'section' => 'home-section'
    ));

    $wp_customize->add_setting('pres-long', array(
        'default' => 'Tutti i giorni aiuto chi ha difficoltà visive a trovare lo strumento adatto a sé, tra lenti e montature delle migliori marche.<br/>Sono anche un fornitore protesico A.S.T. e A.S.S.T dal 1990. Da me troverai anche presidi per l\'ipovisione, dispositivi di protezione individuale e dispositivi medicali ingrandenti.'
    ));

    $wp_customize->add_control('pres-long', array(
        'type' => 'textarea',
        'label' => __( 'Descrizione', 'bootstrap-for-genesis' ),
        'section' => 'home-section'
    ));

    $wp_customize->add_setting('contact', array(
        'default' => 'Vuoi prenotare un incontro o farmi una domanda?'
    ));

    $wp_customize->add_control('contact', array(
        'type' => 'text',
        'label' => __( 'Contattami', 'bootstrap-for-genesis' ),
        'section' => 'home-section'
    ));

    $wp_customize->add_setting('services', array(
        'default' => 'In studio troverai'
    ));

    $wp_customize->add_control('services', array(
        'type' => 'text',
        'label' => __( 'Servizi', 'bootstrap-for-genesis' ),
        'section' => 'home-section'
    ));

    $wp_customize->add_setting('shop', array(
        'default' => 'In studio troverai'
    ));

    $wp_customize->add_control('shop', array(
        'type' => 'text',
        'label' => __( 'Negozio', 'bootstrap-for-genesis' ),
        'section' => 'home-section'
    ));

    $wp_customize->add_section( 'contacts', array(
        'title' => __( 'Recapiti', 'bootstrap-for-genesis' ),
        'priority' => 80,
        'panel' => 'bootstrap'
    ) );

    $wp_customize->add_setting('addr', array(
        'default' => 'vicolo Montalto, Treviglio (BG)'
    ));

    $wp_customize->add_control('addr', array(
        'type' => 'text',
        'label' => __( 'Indirizzo', 'bootstrap-for-genesis' ),
        'section' => 'contacts'
    ));

    $wp_customize->add_setting('time-long', array(
        'default' => 'da Martedì a Sabato<br/>9.00-12.00<br/>14.00-19-00'
    ));

    $wp_customize->add_control('time-long', array(
        'type' => 'text',
        'label' => __( 'Orari', 'bootstrap-for-genesis' ),
        'section' => 'contacts'
    ));
} );
