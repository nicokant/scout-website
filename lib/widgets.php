<?php

class contact_widget extends WP_Widget {
  function __construct() {
    parent::__construct(
      'jungle_voyage_contact',
      'Contatto',
      array(
        'description' => 'Contatto di un capo unità',
      ));
  }

  public function widget($args, $instance) {
    echo "<div class='d-flex align-items-center contact_widget mb-2'><i class='fl-backpack mx-3'></i>";
    echo '<div><div><b>'. $instance['name'] .'</b><br /> '. $instance['role'] .'</div><div>'. $instance['tel'] . '<br/>' .$instance['email'] . '</div></div></div>';
  }

  public function form($instance) {
    if (isset($instance['name'])) {
      $name = $instance['name'];
    } else {
      $name = '';
    }

    if (isset($instance['role'])) {
      $role = $instance['role'];
    } else {
      $role = '';
    }

    if (isset($instance['tel'])) {
      $tel = $instance['tel'];
    } else {
      $tel = '';
    }

    if (isset($instance['email'])) {
      $email = $instance['email'];
    } else {
      $email = '';
    }

    ?>
      <p>
        <label for="<?php echo $this->get_field_id('name');?>">Nome:</label>
        <input
          class="widefat"
          type="text"
          id="<?php echo $this->get_field_id( 'name' ); ?>"
          name="<?php echo $this->get_field_name( 'name' ); ?>"
          value="<?php echo esc_attr( $name ); ?>"
        />
      </p>

      <p>
        <label for="<?php echo $this->get_field_id('tel');?>">Telefono:</label>
        <input
          class="widefat"
          type="text"
          id="<?php echo $this->get_field_id( 'tel' ); ?>"
          name="<?php echo $this->get_field_name( 'tel' ); ?>"
          value="<?php echo esc_attr( $tel ); ?>"
        />
      </p>

      <p>
        <label for="<?php echo $this->get_field_id('role');?>">Ruolo:</label>
        <input
          class="widefat"
          type="text"
          id="<?php echo $this->get_field_id( 'role' ); ?>"
          name="<?php echo $this->get_field_name( 'role' ); ?>"
          value="<?php echo esc_attr( $role ); ?>"
        />
      </p>

      <p>
        <label for="<?php echo $this->get_field_id('email');?>">Email:</label>
        <input
          class="widefat"
          type="text"
          id="<?php echo $this->get_field_id( 'email' ); ?>"
          name="<?php echo $this->get_field_name( 'email' ); ?>"
          value="<?php echo esc_attr( $email ); ?>"
        />
      </p>
    <?php
  }

  public function update($new, $old) {
    $instance = $old;

    if (isset($new['name'])) {
        $instance['name'] = $new['name'];
    }

    if (isset($new['tel'])) {
        $instance['tel'] = $new['tel'];
    }

    if (isset($new['role'])) {
      $instance['role'] = $new['role'];
    }

    if (isset($new['email'])) {
      $instance['email'] = $new['email'];
    }

    return $instance;
  }
}

function jungle_voyage_load_service_widget() {
  register_widget('contact_widget');
}

add_action('widgets_init', 'jungle_voyage_load_service_widget');
