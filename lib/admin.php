<?php
add_action('genesis_admin_before_metaboxes', 'remove_metaboxes');
function remove_metaboxes($hook) {
  remove_meta_box('genesis-theme-settings-blogpage', $hook, 'main');
  remove_meta_box('genesis-theme-settings-layout', $hook, 'main');
  remove_meta_box('genesis-theme-settings-breadcrumb', $hook, 'main');
  remove_meta_box('genesis-theme-settings-scripts', $hook, 'main');
  remove_meta_box('genesis-theme-settings-comments', $hook, 'main');
  remove_meta_box('genesis-theme-settings-header', $hook, 'main');
}

add_action('admin_menu', 'remove_genesis_page_scripts_box');
function remove_genesis_page_scripts_box() {
    remove_meta_box('genesis_inpost_scripts_box', 'page', 'normal');
}

add_action('admin_menu', 'remove_genesis_post_scripts_box');
function remove_genesis_post_scripts_box() {
    remove_meta_box('genesis_inpost_scripts_box', 'post', 'normal');
}

add_action('admin_menu', 'remove_genesis_page_post_scripts_box');
function remove_genesis_page_post_scripts_box() {
    $types = array('post', 'page');
    remove_meta_box('genesis_inpost_scripts_box', $types, 'normal');
}

remove_action('admin_menu', 'genesis_add_inpost_seo_box');
remove_action('admin_menu', 'genesis_add_inpost_layout_box');
