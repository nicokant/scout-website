<?php
/**
 * Search Form
 *
 * @package      Bootstrap for Genesis
 * @since        1.0
 * @link         http://webdevsuperfast.github.io
 * @author       Rotsen Mark Acob <webdevsuperfast.github.io>
 * @copyright    Copyright (c) 2017, Rotsen Mark Acob
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
*/

add_filter( 'get_search_form', 'bfg_search_form' );
function bfg_search_form( $form ) {
    $form = '<form class="form search-form" role="search" method="get" id="searchform" action="' . home_url('/') . '" >
    <div class="form-group d-flex flex-column flex-lg-row">
	<input class="form-control mr-2" type="text" value="' . get_search_query() . '" placeholder="' . esc_attr__('Search', 'bootstrap-for-genesis') . '..." name="s" id="s" />
    <button type="submit" id="searchsubmit" value="'. esc_attr__('Search', 'bootstrap-for-genesis') .'" class="btn btn-primary py-0 mt-2 mt-lg-0 "><i data-feather="search" height="20" width="20" ></i></button>
    </div>
    </form>';
    return $form;
}
