<?php

function jungle_voyage_setup_center_content_layout() {
  remove_action( 'genesis_after_content', 'genesis_get_sidebar' );
  remove_action( 'genesis_after_content', 'genesis_get_sidebar_alt' );
}

genesis_register_layout( 'center-content', array(
  'label' => 'Content Center'
) );

function __jungle_voyage_return_center_content() {
  return 'center-content';
}

// add_action('genesis_before', 'jungle_voyage_setup_center_content_layout');
