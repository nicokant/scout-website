<?php
/**
 * Custom Header
 *
 * @package      Bootstrap for Genesis
 * @since        1.0
 * @link         http://webdevsuperfast.github.io
 * @author       Rotsen Mark Acob <webdevsuperfast.github.io>
 * @copyright    Copyright (c) 2015, Rotsen Mark Acob
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

add_image_size('banner', 1200, 200, true);

function create_small_navbar()
{
    ?>
    <div class="py-2 d-none d-sm-block bg-secondary">
      <div class="d-sm-flex d-none justify-content-between align-content-center container">
        <div class="d-flex align-items-center text-white">
          <i data-feather="clock"></i>
          <span class="ml-2 text-muted"><?php echo get_theme_mod('time'); ?></span>
        </div>
        <div class="d-flex align-items-center">
          <a href="tel:<?php echo get_theme_mod('phone'); ?>" class="d-flex align-items-center ml-3 text-white">
            <i data-feather="phone"></i>
            <span class="ml-2 text-muted"><?php echo get_theme_mod('phone'); ?></span>
          </a>
        </div>
      </div>
    </div>
  <?php
}

function jungle_after_header () {
  ?>
    <div class="">
      <div class="hero text-white no-main">
        <img src="<?php print(get_stylesheet_directory_uri());?>/images/hero.png" />
        <h1 class="display-1 font-weight-light"><?php bloginfo('name');?></h1>
      </div>
    </div>
    <div style="min-height:200px;"></div>
    <div class="band rtl band-primary-alpha"></div>
  <?php
}

function jungle_before_footer () {
  ?>
    <div class="band ltr band-flat"></div>
  <?php
}


// Custom Header
remove_action('wp_head', 'genesis_custom_header_style');
remove_action('genesis_header', 'genesis_do_header');

//add_action('genesis_before_header', 'create_small_navbar');
add_action('genesis_after_header', 'jungle_after_header');
add_action('genesis_before_footer', 'jungle_before_footer', 1);

