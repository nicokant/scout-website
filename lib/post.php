<?php

function jungle_voyage_post_info_filter($post_info) {
  return '[post_date]';
}

add_filter('genesis_post_info', 'jungle_voyage_post_info_filter');

function custom_post_meta($post_meta)
{
  $post_meta='[[post_categories before=""]] ';
  return $post_meta;
}
add_filter('genesis_post_meta', 'custom_post_meta ');
