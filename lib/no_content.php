<?php

function jungle_genesis_no_content() {
	jungle_genesis_header();
	jungle_genesis_footer();
}
//Customised Genesis Header
function jungle_genesis_header() {
	do_action( 'genesis_doctype' );
	do_action( 'genesis_title' );
	do_action( 'genesis_meta' );

	wp_head(); //* we need this for plugins
	?>
	</head>
	<?php
	genesis_markup( array(
		'html5'   => '<body %s>',
		'xhtml'   => sprintf( '<body class="%s">', implode( ' ', get_body_class() ) ),
		'context' => 'body',
	) );
	do_action( 'genesis_before' );

	genesis_markup( array(
		'html5'   => '<div %s>',
		'xhtml'   => '<div id="wrap">',
		'context' => 'site-container',
	) );

	do_action( 'genesis_before_header' );
	do_action( 'genesis_header' );
	do_action( 'genesis_after_header' );

	//genesis_markup( array(
		//'html5'   => '<div %s>',
		//'xhtml'   => '<div id="inner">',
		//'context' => 'site-inner',
	//) );
	//genesis_structural_wrap( 'site-inner' );
}
//Customised Genesis Footer
function jungle_genesis_footer() {
	//genesis_structural_wrap( 'site-inner', 'close' );
	//echo '</div>'; //* end .site-inner or #inner

	do_action( 'genesis_before_footer' );
	do_action( 'genesis_footer' );
	do_action( 'genesis_after_footer' );

	echo '</div>'; //* end .site-container or #wrap

	do_action( 'genesis_after' );
	wp_footer(); //* we need this for plugins
	?>
	</body>
	</html>
<?php
	}
