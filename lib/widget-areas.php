<?php
/**
 * Widget Areas
 *
 * @package      Bootstrap for Genesis
 * @since        1.0
 * @link         http://webdevsuperfast.github.io
 * @author       Rotsen Mark Acob <webdevsuperfast.github.io>
 * @copyright    Copyright (c) 2015, Rotsen Mark Acob
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
*/

function jungle_voyage_register_services() {
 	genesis_register_sidebar(array(
		'id' => 'units-widget-area',
		'name' => __('Contatti', 'bootstrap-for-genesis'),
		'description' => __('Sezione con lista di contati', 'bootstrap-for-genesis')
	));


	genesis_register_sidebar(array(
		'id' => 'contact-widget-area',
	  'name' => __('Contattami', 'bootstrap-for-genesis'),
  	'description' => __('Sezione per richiesta di contatto', 'bootstrap-for-genesis'),
	));
}

add_action('init', 'jungle_voyage_register_services');
