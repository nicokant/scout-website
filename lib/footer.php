<?php
/**
 * Custom Footer
 *
 * @package      Bootstrap for Genesis
 * @since        1.0
 * @link         http://webdevsuperfast.github.io
 * @author       Rotsen Mark Acob <webdevsuperfast.github.io>
 * @copyright    Copyright (c) 2015, Rotsen Mark Acob
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
*/


function jungle_voyage_appointments() {
  if (!is_front_page()) {
  ?>
  <div class="bg-success text-white pt-5 pb-3 text-center">
    <div class="container">
      <div class="row align-items-center flex-column-reverse flex-md-row">
        <div class="col-md-6">
          <?php genesis_widget_area('contact-widget-area', array(
            'before' => '<div id="contact-me-widget" class="px-5 py-3">',
            'after' => '</div>',
          )); ?>
        </div>
        <div class="col-md-6">
          <h2 class="display-4">
            <?php echo get_theme_mod('contact'); ?>
          </h2>
        </div>
      </div>


    </div>
  </div>
  <?php }
}

// add_action('genesis_before_footer', 'jungle_voyage_appointments', 0);

add_filter('genesis_footer_creds_text', 'jungle_voyage_footer_creds_filter');

function jungle_voyage_footer_creds_filter( $creds ) {
	$creds = 'Scout Gorgonzola [footer_copyright] - Tutti i diritti riservati &middot; <a href="/privacy">Privacy</a> &middot; <a href="/credits" title="Credits">Credits</a>';
	return $creds;
}
