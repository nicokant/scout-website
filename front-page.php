<?php

add_image_size('home-thumbnail', 400, 200, true);
add_image_size('logo', 100, 100, true);

function jungle_voyage_hero_image()
{
    ?>
<div class="hero-section">
  <div class="hero text-white">
    <img src="<?php print(get_stylesheet_directory_uri());?>/images/hero.png" />
    <h1 class="display-1 font-weight-light"><?php bloginfo('name');?></h1>
    <h4 class="mb-5 font-weight-light"><?php bloginfo('description');?></h4>
  </div>
</div>
<?php
}

function jungle_voyage_services_section()
{
    ?>
  <div class="band ltr band-secondary-only"></div>
  <div class="pt-5 pb-5 bg-secondary-alpha">
    <div class="container">
      <div class="text-center mw-50">
        <h2 class="display-4 font-weight-light"><?php echo get_theme_mod('pres'); ?></h2>
        <p><?php echo get_theme_mod('pres-long'); ?></p>
      </div>
    </div>
  </div>
  <div class="band rtl band-secondary-only"></div>


  <div class="">

    <div class="pb-3 pt-5 text-center container text-light">
      <h2 class="display-4 font-weight-light"><?php echo get_theme_mod('services'); ?></h2>
      <div id="services-block" class="pb-0 pb-md-5 pt-2 row">
        <div class="py-3 mt-2 col-12 col-md-4 col-sm-6">
          <div class="text-center mb-3">
            <img src="<?php print(get_stylesheet_directory_uri());?>/images/branco.jpg" class="rounded-circle my-2 mx-autom mw-adapt" alt="lupetti">
            <h4>Lupetti</h4>
            <p>Da 8 a 11 anni</p>
            <a class="btn btn-warning" href="<?php echo get_theme_mod('lupi-link'); ?>">
              <?php echo get_theme_mod('link-button-pages'); ?>
            </a>
          </div>
        </div>

        <div class="py-3 my-2 col-12 col-md-4 col-sm-6">
          <div class="text-center mb-3">
            <img src="<?php print(get_stylesheet_directory_uri());?>/images/riparto.jpg" class="rounded-circle my-2 mx-auto mw-adapt" alt="lupetti">
            <h4>Riparto</h4>
            <p>Da 11 a 16 anni</p>
            <a class="btn btn-secondary" href="<?php echo get_theme_mod('esplo-link'); ?>">
              <?php echo get_theme_mod('link-button-pages'); ?>
            </a>
          </div>
        </div>

        <div class="py-3 col-12 my-2 col-md-4 col-sm-6">
          <div class="text-center mb-3">
            <img src="<?php print(get_stylesheet_directory_uri());?>/images/clan.jpg" class="rounded-circle my-2 mx-auto mw-adapt" alt="lupetti">
            <h4>Clan</h4>
            <p>Da 16 a 21 anni</p>
            <a class="btn btn-danger" href="<?php echo get_theme_mod('clan-link'); ?>">
              <?php echo get_theme_mod('link-button-pages'); ?>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <?php do_action('jungle_voyage_presentation_divider');?>

<?php
}

function jungle_voyage_create_appointments()
{
?>
  <div class="band ltr band-secondary-only"></div>
  <div class="bg-secondary-alpha pt-5 pb-5 text-center">
    <div class="container">
      <div class="row align-items-center flex-column-reverse flex-md-row">
        <div class="col-md-6">
          <?php genesis_widget_area('contact-widget-area', array(
        'before' => '<div id="contact-me-widget" class="px-5 py-3">',
        'after' => '</div>',
    ));?>
        </div>
        <div class="col-md-6">
          <i class="fl-compass big-font"></i>
          <h2 class="display-4 font-weight-light">
            <?php echo get_theme_mod('contact'); ?>
          </h2>
        </div>
      </div>


    </div>
  </div>
  <div class="band rtl band-secondary-only"></div>

<?php


}

function jungle_voyage_before_loop()
{
    ?>
    <div class="py-5 container">
      <h1 class="text-center mb-5 display-4">Articoli Recenti</h1>
  <?php
}

function jungle_voyage_before_while()
{
    echo '<div class="py-3 row">';
}

function jungle_voyage_after_while()
{
    echo '</div><div class="text-center"><button class="btn btn-success">Leggi di più</button></div>';
}

function jungle_voyage_after_loop()
{
    ?>
    </div>
  <?php
}

function jungle_voyage_before_entry()
{
    ?>
    <div class="col-12 col-sm-4">
      <div class="mb-3 mb-sm-0 text-center text-sm-left">
  <?php
}

function jungle_voyage_after_entry()
{
    ?>
      </div>
    </div>
  <?php
}

function jungle_voyage_image_before_entry()
{
    $image = genesis_get_image(array(
        'size' => 'home-thumbnail',
        'format' => 'html',
        'attr' => array(
            'alt' => the_title_attribute('echo=0'),
            'class' => 'mw-100',
        ),
    ));
    if ($image) {
        printf('<a class="featured-image" href="%s" rel="bookmark">%s</a>', get_permalink(), $image);
    }
}

function jungle_voyage_do_loop()
{
    global $post;

    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 2,
        'post_status' => 'publish',
    );

    global $wp_query;
    $wp_query = new WP_Query($args);
    do_action('genesis_before_while');
    if (have_posts()): while (have_posts()): the_post();
            do_action('genesis_before_entry');

            printf('<article %s>', genesis_attr('entry'));

            do_action('genesis_entry_header');

            do_action('genesis_before_entry_content');
            do_action('genesis_after_entry_content');

            do_action('genesis_entry_footer');

            echo '</article>';

            do_action('genesis_after_entry');
            $loop_counter++;
        endwhile;
        do_action('genesis_after_endwhile');
    else:
        do_action('genesis_loop_else');
    endif;

    wp_reset_query();
}

function jungle_voyage_do_map()
{
    ?>
    <div id="map" class="map-wrapper"></div>
    <script>
      function initMap() {
        var coords = {
          lat: 45.5323493,
          lng: 9.4031023,
        }

        var map = new google.maps.Map(document.getElementById('map'), {
          center: coords,
          zoom: 16,
          disableDefaultUI: true,
        });

        var marker = new google.maps.Marker({
          position: coords,
          map,
          title: 'Sede gruppo Gorgonzola I'
        });
        marker.set(map);
      }
    </script>
  <?php
}

function jungle_voyage_before_footer()
{
    ?>

    <div class="no-gutters row">
      <div class="col-12 col-sm-6 d-flex justify-content-center align-items-center">
        <div class="py-3 py-md-0 pl-4 pl-md-0 text-white">
        <h2 class="display-4 font-weight-light"><?php echo get_theme_mod('shop'); ?></h2>
          <p class="align-icon">
            <i data-feather="map-pin" class="mr-2"></i><?php echo get_theme_mod('addr'); ?>
          </p>
          <div class="d-flex align-items-start justify-content-start">
            <i data-feather="clock" class="mr-2"></i>
            <div class="d-inline-block"><?php echo get_theme_mod('time-long'); ?></div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <?php jungle_voyage_do_map();?>
      </div>
    </div>
  <?php
}

function jungle_front_before_footer () {
  ?>
    <div class="band ltr band-secondary-only-flat"></div>
  <?php
}


add_filter('genesis_post_title_output', 'jungle_voyage_post_title_output', 15);
function jungle_voyage_post_title_output($title)
{
    $title = sprintf('<h3><a href="%s"; title="%s">%s</a></h3>', get_permalink(), the_title_attribute('echo=0'), the_title_attribute('echo=0'));
    return $title;
}

add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');
add_filter('genesis_markup_site-inner', '__return_null');
remove_action('genesis_before_loop', 'genesis_do_breadcrumbs');

remove_action('genesis_after_header', 'jungle_voyage_carousel', 1);

add_action('genesis_after_header', 'jungle_voyage_hero_image', 1);
add_action('genesis_after_header', 'jungle_voyage_services_section', 2);

add_action('genesis_after_header', 'jungle_voyage_before_footer', 4);

add_action('jungle_voyage_presentation_divider', 'jungle_voyage_create_appointments');

add_action('genesis_loop', 'jungle_voyage_do_loop');
// remove_action('genesis_loop', 'genesis_do_loop');

/* add_action('genesis_before_loop', 'jungle_voyage_before_loop');
add_action('genesis_after_loop', 'jungle_voyage_after_loop'); */

add_action('genesis_before_while', 'jungle_voyage_before_while');
add_action('genesis_after_endwhile', 'jungle_voyage_after_while');
remove_action('genesis_after_endwhile', 'genesis_posts_nav');

add_action('genesis_before_entry', 'jungle_voyage_before_entry');
add_action('genesis_after_entry', 'jungle_voyage_after_entry');

remove_action('genesis_entry_content', 'genesis_do_post_content');
remove_action('genesis_entry_header', 'genesis_do_post_title');
//remove_action('genesis_entry_header', 'genesis_post_info', 12);
remove_action('genesis_entry_footer', 'genesis_post_meta');

remove_action('genesis_after_header', 'jungle_after_header');
remove_action('genesis_before_footer', 'jungle_before_footer', 1);
add_action('genesis_before_footer', 'jungle_front_before_footer', 1);



add_action('genesis_entry_header', 'jungle_voyage_image_before_entry');
add_action('genesis_entry_header', 'genesis_do_post_title');

jungle_genesis_no_content()
?>
